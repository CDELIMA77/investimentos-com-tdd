package com.investimentos.services;

import com.investimentos.models.Investimento;
import com.investimentos.models.Simulacao;
import com.investimentos.models.SimulacaoR;
import com.investimentos.repositories.SimulacaoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SimulacaoServices {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    InvestimentoServices investimentoServices;

    public SimulacaoR registrarSimulacao(Simulacao simulacao) throws ObjectNotFoundException {

        Optional<Investimento> investimentoOptional = investimentoServices.buscarPorId(simulacao.getInvestimento().getId());

        if (investimentoOptional.isPresent()){
            simulacao.setRetornoSimulado(calcularRetorno(simulacao));
            Simulacao simulacao3 = simulacaoRepository.save(simulacao);
            /* Retornar JSON só com o valor na resposta */
            SimulacaoR simulacaoR = new SimulacaoR();
            simulacaoR.setRetorno(simulacao.getRetornoSimulado());
            return simulacaoR;
        }
        throw new ObjectNotFoundException(Investimento.class, "Investimento não encontrado");
    }

    public Double calcularRetorno(Simulacao simulacao){
        Double indice = Math.pow((1 + (simulacao.getInvestimento().getRentabilidadeMes()/100)),simulacao.getMesesDeAplicacao());

        /*C = inicial * (1+i)^n */
        Double retornoEsperado = simulacao.getDinheiroAplicado() * indice;

        return retornoEsperado;
    }
}
