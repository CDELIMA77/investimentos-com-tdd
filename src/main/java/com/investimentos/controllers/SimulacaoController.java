package com.investimentos.controllers;

import com.investimentos.models.Simulacao;
import com.investimentos.models.SimulacaoR;
import com.investimentos.services.SimulacaoServices;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {

    @Autowired
    private SimulacaoServices simulacaoServices;

    @PutMapping()
    public ResponseEntity<SimulacaoR> realizarSimulacao(@RequestBody @Valid Simulacao simulacao){
        SimulacaoR simulacaoObjetoR;
        try{simulacaoObjetoR = simulacaoServices.registrarSimulacao(simulacao);}
        catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.status(200).body(simulacaoObjetoR);
    }
}
