package com.investimentos.models;

public class SimulacaoR {

    private Double retorno;

    public SimulacaoR() {
    }

    public SimulacaoR(Double retorno) {
        this.retorno = retorno;
    }

    public Double getRetorno() {
        return retorno;
    }

    public void setRetorno(Double retorno) {
        this.retorno = retorno;
    }
}
