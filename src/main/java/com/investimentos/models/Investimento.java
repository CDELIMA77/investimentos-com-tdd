package com.investimentos.models;

import com.investimentos.enums.RiscoDoInvestimento;
import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Cadastro_Investimentos")
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nome_completo")
    @NotNull
    @Size(min=1, max=100, message = "Nome deve ter entre 1 e 100 caracteres")
    private String nome;

    @NotNull
    /* @NotEmpty é quando vc nao quer aceitar vazio */
    @Size(min=1, max=100, message = "Descrição deve ter entre 1 e 100 caracteres")
    private String descricao;

    private RiscoDoInvestimento tipoDeRisco;

    @NotNull
    /* é ao mes */
    @DecimalMin("0.1")
    /* Tem também o @DecimalMax, @Min, @Max */
    Double rentabilidadeMes;

    public Investimento() {
    }

    public Investimento(Integer id, @NotNull @Size(min = 1, max = 100, message = "Nome deve ter entre 1 e 100 caracteres") String nome, @NotNull @Size(min = 1, max = 100, message = "Descrição deve ter entre 1 e 100 caracteres") String descricao, RiscoDoInvestimento tipoDeRisco, @NotNull @DecimalMin("0.1") Double rentabilidadeMes) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.tipoDeRisco = tipoDeRisco;
        this.rentabilidadeMes = rentabilidadeMes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public RiscoDoInvestimento getTipoDeRisco() {
        return tipoDeRisco;
    }

    public void setTipoDeRisco(RiscoDoInvestimento tipoDeRisco) {
        this.tipoDeRisco = tipoDeRisco;
    }

    public Double getRentabilidadeMes() {
        return rentabilidadeMes;
    }

    public void setRentabilidadeMes(Double rentabilidadeMes) {
        this.rentabilidadeMes = rentabilidadeMes;
    }
}
