package com.investimentos.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.investimentos.enums.RiscoDoInvestimento;
import com.investimentos.models.Investimento;
import com.investimentos.models.Simulacao;
import com.investimentos.models.SimulacaoR;
import com.investimentos.security.JWTUtil;
import com.investimentos.services.InvestimentoServices;
import com.investimentos.services.SimulacaoServices;
import com.investimentos.services.UsuarioServices;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(SimulacaoController.class)
@Import(JWTUtil.class)
public class SimulacaoControllerTests {

    @MockBean
    SimulacaoServices simulacaoServices;

    @MockBean
    UsuarioServices usuarioService;

    @MockBean
    InvestimentoServices investimentoServices;

    @Autowired
    private MockMvc mockMvc;

    Simulacao simulacao;
    SimulacaoR simulacaoR;
    Investimento investimento;

    @BeforeEach
    public void inicializar() {
        investimento = new Investimento();
        simulacao = new Simulacao();
        simulacaoR = new SimulacaoR();
        simulacao.setEmail("cynthia@gmail.com");
        investimento.setId(1);
        investimento.setNome("POUPANÇA");
        investimento.setDescricao("POUPANÇA");
        investimento.setRentabilidadeMes(0.30);
        investimento.setTipoDeRisco(RiscoDoInvestimento.BAIXO);
        simulacao.setInvestimento(investimento);
        simulacao.setMesesDeAplicacao(4);
        simulacao.setDinheiroAplicado(200.00);
    }

    ObjectMapper mapper = new ObjectMapper();

    @Test
    @WithMockUser(username= "usuario@gmail.com", password = "aviao123")
    public void testarRealizarSimulacao() throws Exception {
        Mockito.when(simulacaoServices.calcularRetorno(Mockito.any(Simulacao.class))).thenReturn(simulacao.getRetornoSimulado());
        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                /*.andExpect(MockMvcResultMatchers.jsonPath("$.RetornoSimulado",
                        CoreMatchers.equalTo(202.4108216161999)))*/  ;
    }
}
