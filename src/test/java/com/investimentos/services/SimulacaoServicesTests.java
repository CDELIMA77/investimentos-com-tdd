package com.investimentos.services;

import com.investimentos.enums.RiscoDoInvestimento;
import com.investimentos.models.Investimento;
import com.investimentos.models.Simulacao;
import com.investimentos.models.SimulacaoR;
import com.investimentos.repositories.InvestimentoRepository;
import com.investimentos.repositories.SimulacaoRepository;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class SimulacaoServicesTests {

    @MockBean
    InvestimentoRepository investimentoRepository;

    @Autowired
    InvestimentoServices investimentoServices;

    @MockBean
    SimulacaoRepository simulacaoRepository;

    @Autowired
    SimulacaoServices simulacaoServices;

    Investimento investimento;
    Simulacao simulacao;
    SimulacaoR simulacaoR;

    @BeforeEach
    public void inicializar() {
        simulacao = new Simulacao();
        simulacaoR = new SimulacaoR();
        investimento = new Investimento();
        simulacao.setEmail("cynthia@gmail.com");
        investimento.setId(1);
        investimento.setNome("POUPANÇA");
        investimento.setDescricao("POUPANÇA");
        investimento.setRentabilidadeMes(0.30);
        investimento.setTipoDeRisco(RiscoDoInvestimento.BAIXO);
        simulacao.setInvestimento(investimento);
        simulacao.setMesesDeAplicacao(4);
        simulacao.setDinheiroAplicado(200.00);
        /*simulacao.setRetornoSimulado(202.4108216161999); */
    }

    @Test
    public void testarRegistrarSimulacao()  {
        Optional<Investimento> investimentoOptional = Optional.of(investimento);
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptional);

        Mockito.when(simulacaoRepository.save(Mockito.any(Simulacao.class))).thenReturn(simulacao);
        simulacaoServices.registrarSimulacao(simulacao);

        Mockito.verify(simulacaoRepository,Mockito.times(1)).save(Mockito.any(Simulacao.class));

    }

    @Test
    public void testarCalcularRetorno() {
        Double retornoEsperado = simulacaoServices.calcularRetorno(simulacao);
        Assertions.assertEquals(202.4108216161999, retornoEsperado);
            }
}